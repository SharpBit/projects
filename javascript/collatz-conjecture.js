/*
Collatz Conjecture - Start with a number n > 1.
Find the number of steps it takes to reach one using the following process:
If n is even, divide it by 2. If n is odd, multiply it by 3 and add 1.
*/

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Type a number above 1: ', (num) => {
    num = parseInt(num)
    if (!(num > 1)) {
        console.log('That number is not above 1!');
        rl.close()
    }
    let steps = 0
    while (!(num == 1)) {
        num = (num % 2 == 0) ? num / 2 : num * 3 + 1;
        steps++;
    };
    console.log(`Steps: ${steps}`)
    rl.close();
});
