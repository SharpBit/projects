/*
Pig Latin is a game of alterations played on the English language game.
To create the Pig Latin form of an English word the initial consonant sound is transposed to the end of the word and an ay is affixed
Ex.: "banana" would yield anana-bay
Read Wikipedia for more information on rules.
*/

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let pigLatin = () => {
    rl.question('Type a word: ', (word) => {
        if ('aeiou'.includes(word.slice(0, 1).toLowerCase())) {
            word += 'ay';
        } else {
            word = `${word.slice(1)}${word.slice(0, 1)}ay`;
        }
        rl.close();
        console.log(word);
    });
};

pigLatin()
