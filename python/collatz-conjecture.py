num = int(input('Type a number above 1: '))

if num > 1:
    steps = 0
    while num != 1:
        if num % 2 == 0:
            num /= 2
        else:
            num = num * 3 + 1
        steps += 1
    print('Steps: {}'.format(steps))
else:
    print('That number is not above 1!')
