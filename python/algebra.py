'''
Solves any algebra problem.
'''

from sympy.solvers import solve


def parse_equation(equation):
    eq = list(equation.replace('^', '**'))
    to_parse = ''
    for i, char in enumerate(eq):
        if char.isalpha():  # variable
            if eq[i - 1].isdigit():  # the char before is a number
                if i != 0:  # not the first char of the equation
                    to_parse += f'{eq[i-1]}*{eq[i]}'
                else:
                    to_parse += char
            else:
                to_parse += char
        else:  # number
            if i != len(eq) - 1:  # last char
                if eq[i].isdigit():
                    if eq[i + 1].isalpha():
                        continue
            to_parse += char
    return to_parse


def get_result(equation):
    eq = parse_equation(equation)
    result = solve(eq)
    for i, r in enumerate(result):
        result[i] = str(r)
    return result


if __name__ == '__main__':
    equation = input('Enter the equation: ')
    result = get_result(equation)
    print('Equation: {} = 0'.format(equation))
    print('Result(s): {}'.format(', '.join(result)))
