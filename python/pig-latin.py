'''
Pig Latin is a game of alterations played on the English language game.
To create the Pig Latin form of an English word the initial consonant sound is transposed to the end of the word and an ay is affixed
Ex.: "banana" would yield anana-bay
Read Wikipedia for more information on rules.
'''


def check_alpha(word):
    for letter in word:
        if not word.isalpha():
            return False
    return True


def pig_latin(word):
    if word[0].lower() in 'aeiou':
        pig_latin = word + 'ay'
    else:
        pig_latin = word[1:] + word[0] + 'ay'
    return pig_latin


if __name__ == '__main__':
    word = input('Type a word.\n> ')

    if check_alpha(word):
        pig_latin = pig_latin(word)
        print(pig_latin)
    else:
        print('That is not a word!')
