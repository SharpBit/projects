'''
Change Return Program - The user enters a cost and then the amount of money given.
The program will figure out the change and the number of quarters, dimes, nickels, pennies needed for the change.
'''


def get_change(cost, amount):
    change = int((amount - cost) * 100)
    quarters, remainder = divmod(change, 25)
    dimes, remainder = divmod(remainder, 10)
    nickels, remainder = divmod(remainder, 5)
    pennies = remainder

    return (quarters, dimes, nickels, pennies)


if __name__ == '__main__':
    cost = float(input('How much is the cost?\n> '))
    amount = float(input('How much was paid?\n> '))

    change = get_change(cost, amount)
    print('The change is {0[0]} quarters, {0[1]} dimes, {0[2]} nickles, and {0[3]} pennies.'.format(change))
