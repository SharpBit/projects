# Projects
These projects are inspired by the popular list of programming projects located [here](https://github.com/karan/Projects) and just basics of languages I am learning.

### Purpose
This is so I can practice different languages such as Python and JavaScript, what I am currently learning/working on now.

### Feedback
If you spot an error or better way to do something, feel free to create a pull request with the new method explaining how each part works. I would love to learn so I can stop using messy code.
